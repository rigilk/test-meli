import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Components
import Header from './components/shared/Header';
import Home from './components/Home';
import ItemList from './components/ItemList';
import ItemView from './components/ItemView';

// Import styles
import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss';
import './App.scss';

function App() {
  return (
    <Router>
      <div className="App">
        <Header />

        <div className="row">
          <div className="col-md-10 col-md-offset-1">
            <div className="main-container">
              <Route exact path="/" component={Home} />
              <Route exact path="/items" component={ItemList} />
              <Route exact path="/items/view" component={ItemView} />
            </div>
          </div>
        </div>

        <div class="panel panel-navigation">
          <div class="panel-body">
            Test Navigation
            <ul class="nav nav-pills nav-stacked">
              <li role="presentation"><a href="/">Home</a></li>
              <li role="presentation"><a href="/items/">List</a></li>
              <li role="presentation"><a href="/items/view/">View</a></li>
            </ul>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
