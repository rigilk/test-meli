import React, { Component } from 'react';


const SearchBox = props => {
  return(
    <div className="input-group input-group-lg">
      <input type="text"
        className="form-control"
        aria-label="Nunca dejes de buscar"
        placeholder="Nunca dejes de buscar" />
      <span className="input-group-btn">
        <button className="btn btn-default" type="button">
          <img src={process.env.PUBLIC_URL + '/img/ic_Search.png'} />
        </button>
      </span>
    </div>
  )
};

class Header extends Component {
  render() {
    return (
      <header className="navbar navbar-fixed-top">
        <div className="row">
          <div className="col-md-10 col-md-offset-1">
            <div className="navbar-container">
              <div className="navbar-header">
                <a className="navbar-brand" href="/">
                  <img src={process.env.PUBLIC_URL + '/img/Logo_ML.png'} />
                </a>
              </div>
              <form className="navbar-form">
                <SearchBox />
              </form>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
