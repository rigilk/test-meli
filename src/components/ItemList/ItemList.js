import React, { Component } from 'react';

class ItemList extends Component {
  constructor(props){
  super(props);
    this.state = {
      results: [],
    }
  }

  componentDidMount(){
    this.fetchData();
  }

  fetchData(){
      const url = 'https://api.mercadolibre.com/sites/MLA/search?q=:fortisip';

      return fetch(url)
        .then(response => response.json())
        .then(parsedJSON => this.setState({results: parsedJSON.results}))
        .catch(error => console.log(error));
  }

  render() {
    function ItemList(props) {
      const results = props.results;

      const listItems = results.map((result) =>
        <li key={result.toString()}
            className="list-item">
          <div className="media">
            <div className="media-left">
              <a href="/items/view/" title={result.title}>
                <img
                  src={result.thumbnail}
                  alt={result.title}
                  className="media-object"
                  width="180" />
              </a>
            </div>

            <div className="media-body">
              <div className="row">
                <div className="col-xs-8">
                  <h3 className="list-item-value">
                    ${result.price} {result.shipping.free_shipping ? <img src={process.env.PUBLIC_URL + '/img/ic_shipping.png'} /> :''}
                  </h3>
                  <p>
                    <a href="/items/view/" title={result.title}>{result.title}</a>
                  </p>
                </div>

                <div className="col-xs-4">
                  <p>
                    <small>{result.address.state_name}</small>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </li>
      );

      return (
        <ul className="list-unstyled">
          {listItems}
        </ul>
      );
    }

    return (
      <div>
        <ol className="breadcrumb">
            <li><a href="/">Home</a></li>
            <li className="active">List</li>
          </ol>

        <div className="content-box">
          <ItemList
            results={this.state.results} />
        </div>
      </div>
    );
  }
}

export default ItemList;
