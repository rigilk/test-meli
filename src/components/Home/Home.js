import React, { Component } from 'react';

class Home extends Component {
  render() {
    return (
      <div>
        <div class="alert alert-info" role="alert">
          <p>You are running this application in <b>{process.env.NODE_ENV}</b> mode.</p>
        </div>

        <div class="alert alert-info" role="alert">
          <p>In the bottom of the screen to the left wou will a box that will allow you to successfully navigate the site. <strong>Use it!</strong></p>
        </div>
      </div>
    );
  }
}

export default Home;
