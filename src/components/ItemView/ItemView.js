import React, { Component } from 'react';

class ItemView extends Component {
  constructor(props){
  super(props);
    this.state = {
      dataView: {
        "id": "MLA680611559",
        "site_id": "MLA",
        "title": "Fortisip Max Maxima Energia Lata X 350grs",
        "subtitle": null,
        "seller_id": 191047873,
        "category_id": "MLA8834",
        "official_store_id": 1359,
        "price": 410,
        "base_price": 410,
        "original_price": null,
        "currency_id": "ARS",
        "initial_quantity": 100,
        "available_quantity": 1,
        "sold_quantity": 50,
        "sale_terms": [],
        "buying_mode": "buy_it_now",
        "listing_type_id": "gold_special",
        "start_time": "2017-08-28T18:12:49.000Z",
        "stop_time": "2037-08-23T18:12:49.000Z",
        "condition": "new",
        "permalink": "https://articulo.mercadolibre.com.ar/MLA-680611559-fortisip-max-maxima-energia-lata-x-350grs-_JM",
        "thumbnail": "http://mla-s2-p.mlstatic.com/940583-MLA31114474272_062019-I.jpg",
        "secure_thumbnail": "https://mla-s2-p.mlstatic.com/940583-MLA31114474272_062019-I.jpg",
        "pictures": [
        {
        "id": "940583-MLA31114474272_062019",
        "url": "http://mla-s2-p.mlstatic.com/940583-MLA31114474272_062019-O.jpg",
        "secure_url": "https://mla-s2-p.mlstatic.com/940583-MLA31114474272_062019-O.jpg",
        "size": "404x360",
        "max_size": "404x360",
        "quality": ""
        },
        {
        "id": "624089-MLA25922337928_082017",
        "url": "http://mla-s2-p.mlstatic.com/624089-MLA25922337928_082017-O.jpg",
        "secure_url": "https://mla-s2-p.mlstatic.com/624089-MLA25922337928_082017-O.jpg",
        "size": "500x500",
        "max_size": "1200x1200",
        "quality": ""
        },
        {
        "id": "724520-MLA25922345740_082017",
        "url": "http://mla-s2-p.mlstatic.com/724520-MLA25922345740_082017-O.jpg",
        "secure_url": "https://mla-s2-p.mlstatic.com/724520-MLA25922345740_082017-O.jpg",
        "size": "500x500",
        "max_size": "1200x1200",
        "quality": ""
        },
        {
        "id": "769303-MLA25922344349_082017",
        "url": "http://mla-s2-p.mlstatic.com/769303-MLA25922344349_082017-O.jpg",
        "secure_url": "https://mla-s2-p.mlstatic.com/769303-MLA25922344349_082017-O.jpg",
        "size": "500x500",
        "max_size": "1200x1200",
        "quality": ""
        },
        {
        "id": "914133-MLA25922344323_082017",
        "url": "http://mla-s2-p.mlstatic.com/914133-MLA25922344323_082017-O.jpg",
        "secure_url": "https://mla-s2-p.mlstatic.com/914133-MLA25922344323_082017-O.jpg",
        "size": "500x500",
        "max_size": "1200x1200",
        "quality": ""
        },
        {
        "id": "957046-MLA25922342811_082017",
        "url": "http://mla-s2-p.mlstatic.com/957046-MLA25922342811_082017-O.jpg",
        "secure_url": "https://mla-s2-p.mlstatic.com/957046-MLA25922342811_082017-O.jpg",
        "size": "500x500",
        "max_size": "1200x1200",
        "quality": ""
        }
        ],
        "video_id": null,
        "descriptions": [
        {
        "id": "MLA680611559-1427423420"
        }
        ],
        "accepts_mercadopago": true,
        "non_mercado_pago_payment_methods": [],
        "shipping": {
        "mode": "me2",
        "methods": [],
        "tags": [
        "self_service_in"
        ],
        "dimensions": null,
        "local_pick_up": true,
        "free_shipping": false,
        "logistic_type": "cross_docking",
        "store_pick_up": true
        },
        "international_delivery_mode": "none",
        "seller_address": {
        "city": {
        "name": "Belgrano"
        },
        "state": {
        "id": "AR-C",
        "name": "Capital Federal"
        },
        "country": {
        "id": "AR",
        "name": "Argentina"
        },
        "search_location": {
        "neighborhood": {
        "id": "TUxBQkJFTDcyNTJa",
        "name": "Belgrano"
        },
        "city": {
        "id": "TUxBQ0NBUGZlZG1sYQ",
        "name": "Capital Federal"
        },
        "state": {
        "id": "TUxBUENBUGw3M2E1",
        "name": "Capital Federal"
        }
        },
        "latitude": -34.557205,
        "longitude": -58.463863,
        "id": 168140360
        },
        "seller_contact": null,
        "location": {},
        "geolocation": {
        "latitude": -34.557205,
        "longitude": -58.463863
        },
        "coverage_areas": [],
        "attributes": [
        {
        "id": "BRAND",
        "name": "Marca",
        "value_id": "4285982",
        "value_name": "Nutricia",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "GOALS",
        "name": "Objetivos",
        "value_id": "371072",
        "value_name": "Nutrición",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "ITEM_CONDITION",
        "name": "Condición del ítem",
        "value_id": "2230284",
        "value_name": "Nuevo",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "NUMBER_OF_UNITS_KIT",
        "name": "Cantidad de unidades del kit",
        "value_id": null,
        "value_name": "1",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "PACKAGING_TYPE",
        "name": "Tipo de envase",
        "value_id": "1356648",
        "value_name": "Lata",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "SUPPLEMENTS_NAME",
        "name": "Nombre",
        "value_id": null,
        "value_name": "Fortisip Max",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "SUPPLEMENTS_TYPE",
        "name": "Tipo de suplemento",
        "value_id": null,
        "value_name": "dietario",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "SUPPLEMENT_DETAILED_GOAL",
        "name": "Objetivo detallado",
        "value_id": null,
        "value_name": "producto dietario",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "SUPPLEMENT_DETAILED_TYPE",
        "name": "Tipo de suplemento detallado",
        "value_id": null,
        "value_name": "dietario",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "SUPPLEMENT_FORMAT",
        "name": "Formato del suplemento",
        "value_id": "4567842",
        "value_name": "Polvo",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "TOTAL_CONTENT_VOLUME",
        "name": "Contenido total en volumen",
        "value_id": "-1",
        "value_name": null,
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "TOTAL_CONTENT_WEIGHT",
        "name": "Contenido",
        "value_id": null,
        "value_name": "350 g",
        "value_struct": null,
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        },
        {
        "id": "UNIT_WEIGHT",
        "name": "Peso de la unidad",
        "value_id": "3782",
        "value_name": "350 g",
        "value_struct": {
        "number": 350,
        "unit": "g"
        },
        "attribute_group_id": "OTHERS",
        "attribute_group_name": "Otros"
        }
        ],
        "warnings": [],
        "listing_source": "",
        "variations": [
        {
        "id": 22683559436,
        "price": 410,
        "attribute_combinations": [
        {
        "id": "FLAVOR",
        "name": "Sabor",
        "value_id": "4352471",
        "value_name": "Neutro",
        "value_struct": null
        }
        ],
        "available_quantity": 1,
        "sold_quantity": 50,
        "sale_terms": [],
        "picture_ids": [
        "940583-MLA31114474272_062019",
        "624089-MLA25922337928_082017",
        "724520-MLA25922345740_082017",
        "769303-MLA25922344349_082017",
        "914133-MLA25922344323_082017",
        "957046-MLA25922342811_082017"
        ],
        "catalog_product_id": null
        }
        ],
        "status": "active",
        "sub_status": [],
        "tags": [
        "brand_verified",
        "good_quality_picture",
        "immediate_payment",
        "cart_eligible"
        ],
        "warranty": null,
        "catalog_product_id": null,
        "domain_id": "MLA-SUPPLEMENTS",
        "parent_item_id": null,
        "differential_pricing": null,
        "deal_ids": [],
        "automatic_relist": false,
        "date_created": "2017-08-28T18:12:49.000Z",
        "last_updated": "2019-06-16T00:31:20.000Z",
        "health": 0.7
      }
    }
  }

  render() {
    console.log(this.state.dataView);

    return (
      <div>
        <ol className="breadcrumb">
          <li><a href="/">Home</a></li>
          <li><a href="/items/">List</a></li>
          <li className="active">View</li>
        </ol>

        <div className="content-box">
            <div className="row">
              <div className="col-md-8">
                <img src={this.state.dataView.secure_thumbnail}
                  alt={this.state.dataView.title}
                  width="100%" />
              </div>
              <div className="col-md-4 item-view-details">
                <p>
                  <small>{this.state.dataView.condition} - {this.state.dataView.sold_quantity} vendidos</small>
                </p>
                <h1 className="item-view-title">
                  {this.state.dataView.title}
                </h1>

                <h2 className="item-view-value">
                  $ {this.state.dataView.price}
                </h2>

                <button class="btn btn-primary btn-lg btn-block" type="submit">Comprar</button>
              </div>

              <div className="col-md-8 item-view-description">
                <h3 className="item-view-description-title">Descripci&oacute;n del Producto</h3>
                <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam sem quam, dignissim in elementum sed, suscipit quis ante. Nulla ac egestas neque, sit amet vehicula dui. Suspendisse potenti. Vivamus sit amet pellentesque est. Suspendisse mollis in nisl quis finibus. Phasellus magna tellus, venenatis eget metus sit amet, mollis convallis tellus. Duis tincidunt in mauris et dapibus. Aenean vulputate eleifend urna, nec tempus justo vehicula vitae. Nunc sed leo ut nibh vehicula posuere. Sed lobortis mi sit amet dolor pulvinar, ac elementum erat cursus. Donec ut lobortis mi, ac accumsan odio. Cras quis varius sem. Nam commodo posuere ligula, ac blandit est dignissim eget. Quisque sapien quam, aliquet in lobortis blandit, bibendum id metus. Fusce massa ex, commodo a vestibulum non, consectetur at turpis.</p>
              </div>
            </div>
          </div>
      </div>
    );
  }
}

export default ItemView;
